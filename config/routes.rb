Rails.application.routes.draw do
  get "login" => "sessions#new"
  get "signup" => "users#new"
  get "logout" => "sessions#destroy"

  get 'finances', to: 'finances#index'

  resources :sessions
  resources :users
  resources :people
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get "home/index"  
  root to: "sessions#new"
end
