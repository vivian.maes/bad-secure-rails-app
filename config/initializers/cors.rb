# SECU (A05:2021-Security Misconfiguration) : 
#    Pas une bonne pratique d'ouvrir le CORS pour n'importe quelle origine ; il faut être le plus restrictif possible.
Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins  '*'
    resource '*', headers: :any, methods: [:get, :post]
  end
end