# SECU (A03:2021-Injection) : cross-site-scripting
#    Les entités HTML dans JSON ne sont pas échappées par défaut, important si utilisation de rails comme API
ActiveSupport::JSON::Encoding::escape_html_entities_in_json = false