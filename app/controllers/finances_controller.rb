require 'uri'
require 'net/http'
require 'openssl'

class FinancesController < ApplicationController  
  def index
    # SECU (A04:2021-Insecure Design) :
    #    risque de facilitation de DDOS ressource lourde à créer
    #    il faut instancier les client hors du context app de rails

    # SECU (A05:2021-Security Misconfiguration) : 
    #    on ne peut mettre la clef en claire.
    url = URI("https://www.alphavantage.co/query?keywords=IBM&function=SYMBOL_SEARCH&apikey=Y5B8A5YRWIIDYRVB")
    https = Net::HTTP.new(url.host, url.port)
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    request = Net::HTTP::Get.new(url)
    response = https.request(request)

    @datas = JSON.parse(response.read_body)["bestMatches"]
  end
end  