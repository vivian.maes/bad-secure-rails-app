class ApplicationController < ActionController::Base
  # SECU (A01:2021 – Contrôles d'accès défaillants)
  #   Cross Site Request Forgery: 
  #     Ruby on Rails a un support intégré spécifique pour les jetons CSRF. 
  #     mais il faut l'activer ou vous assurer qu'il est activé
  #     note : si vous utilisez uniquement l'authentification par jeton, il n'est pas nécessaire de se protéger du CSRF dans les contrôleurs
  before_action :authenticated
  helper_method :current_user, :is_admin?

  def current_user
    @current_user ||= User.find_by(id: session[:user_id].to_s)
  end

  def authenticated
     path = request.fullpath.present? ? root_url(url: request.fullpath) : root_url
     redirect_to path and reset_session if !current_user
  end

  def is_admin?
    current_user.admin if current_user
  end
end