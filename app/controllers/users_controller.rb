# frozen_string_literal: true
class UsersController < ApplicationController
  skip_before_action :authenticated, only: [:new, :create]

  def new
    @user = User.new
    render layout: "unconnected", template: "users/new" 
  end

  def create
    
    user = User.new(user_params)
    if user.save
      session[:user_id] = user.id
      redirect_to people_path
    else
      @user = user
      flash[:error] = user.errors.full_messages.to_sentence
      redirect_to :signup
    end
  end

  def account_settings
    @user = current_user
  end

  def update
    message = false

    # SECU (A03:2021-Injection) : 
    #   risque d'injection SQL
    user = User.where("id = '#{params[:user][:id]}'")[0]

    if user
      user.update(user_params_without_password)
      if params[:user][:password].present? && (params[:user][:password] == params[:user][:password_confirmation])
        user.password = params[:user][:password]
      end
      message = true if user.save!
      respond_to do |format|
        format.html { redirect_to user_account_settings_path(user_id: current_user.id) }
        format.json { render json: {msg: message ? "success" : "false "} }
      end
    else
      flash[:error] = "Could not update user!"
      redirect_to root_url(user_id: current_user.id)
    end
  end

  private

  # SECU (A08:2021 – Manque d'intégrité des données et du logiciel) : 
  #    CWE-915: Improperly Controlled Modification of Dynamically-Determined Object Attributes
  def user_params
    params.require(:user).permit!
  end

  def user_params_without_password
    params.require(:user).permit(:email, :first_name, :last_name)
  end
end
