# frozen_string_literal: true
class SessionsController < ApplicationController
  skip_before_action :authenticated, only: [:new, :create]

  def new
    @url = params[:url]
    if current_user
      redirect_to people_path 
    else
      render layout: "unconnected", template: "sessions/new" 
    end
  end

  def create
    path = params[:url].present? ? params[:url] : people_path
    begin
      # Normalize the email address, why not
      user = User.authenticate(params[:email].to_s.strip.downcase, params[:password])
    rescue RuntimeError => e
      # SECU (A09:2021-Security Logging and Monitoring Failures) : 
      #   il faut loguer aussi et même surtout les erreurs d'authentification
    end

    if user
      session[:user_id] = user.id
      redirect_to path
    else
      flash[:error] = e.message
      render layout: "unconnected", template: "sessions/new" 
    end
  end

  def destroy
    cookies.delete(:auth_token)
    reset_session
    redirect_to root_path
  end
end
