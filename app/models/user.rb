class User < ApplicationRecord
  validates :password, presence: true,
                       confirmation: true,
                       length: {within: 6..40},
                       on: :create,
                       if: :password

  validates_presence_of :email
  validates_uniqueness_of :email
  # SECU (CWE-777) : Regular Expression without Anchors
  #   Insufficient validation for email using /.+@.+\..+/i. Use \A and \z 
  validates_format_of :email, with: /.+@.+\..+/i

  before_save :hash_password

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  private

  def self.authenticate(email, password)
    auth = nil
    user = find_by_email(email)
    raise "#{email} doesn't exist!" if !(user)
    # SECU (A02:2021-Cryptographic Failures) :
    #    MD5 est obsolète
    if user.password == Digest::MD5.hexdigest(password)
      auth = user
    else
      raise "Incorrect Password!"
    end
    return auth
  end

  def hash_password
    if will_save_change_to_password?
      # SECU (A02:2021-Cryptographic Failures) :
      #    MD5 est obsolète
      self.password = Digest::MD5.hexdigest(self.password)
    end
  end

end
