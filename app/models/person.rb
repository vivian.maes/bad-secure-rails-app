require 'securerandom'
require 'open-uri'

class Person < ApplicationRecord

  before_save :make_profile_picture

  validates_presence_of  :first_name, :last_name
  validate :url_profile_is_valid

  def url_profile_is_valid
    if self.url_profile && !self.url_profile.empty?
      unless self.url_profile =~ URI::regexp
        img_base_path = Rails.root.join("app/assets/images/person")
        unless File.file?( img_base_path + self.url_profile )
          errors.add(:url_profile, "must be a URI or existing file name")
        end
      end
    end
  end

  #  SECU (A10:2021-Server-Side Request Forgery) : 
  #    Cette Route permet d'injecter des URLs sans la moindre vérification
  #  NOTE : Il y a une deuxiémme faille, ici on écrit directement dans les asset public, 
  #         il faudrait passe par active-storage, mais le code serais moins lisible pour cet exercise.
  #         Je vous laisse jouer avec active-storage.
  def make_profile_picture
    if self.url_profile && !self.url_profile.empty?
      img_base_path = Rails.root.join("app/assets/images/person")
      unless File.file?( img_base_path + url_profile )
        img_name = "#{SecureRandom.uuid}.jpeg"
        File.open("#{img_base_path}/#{img_name}", 'wb') {|file| file.write(URI.open(url_profile).read) }
        self.url_profile = img_name
      end
    end
  end
end